cuda-new-arch-modes
-------------------

* The :prop_tgt:`CUDA_ARCHITECTURES` target property now supports the
  `all`, and `all-major` values for CUDA toolkit 7.0+.

* The :variable:`CMAKE_CUDA_ARCHITECTURES` variable now supports the
  `all`, and `all-major` values for CUDA toolkit 7.0+.
